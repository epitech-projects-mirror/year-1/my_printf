/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "number_printer.h"

typedef char const *conststr;

int my_printf(conststr pattern, ...);

void redirect_all_std(void)
{
    cr_redirect_stdout();
    cr_redirect_stderr();
}

Test (my_printf, simple_string, .init = redirect_all_std){
    my_printf ("hello world");
    cr_assert_stdout_eq_str ("hello world");
}

Test(print_number, print_nulber, .init = redirect_all_std)
{
    put_number(1234);
    cr_assert_stdout_eq("1234");
}


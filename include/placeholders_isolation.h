/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_PLACEHOLDERS_ISOLATION_H
    #define MY_PRINTF_PLACEHOLDERS_ISOLATION_H

    #include "my_printf.h"

int placeholder_length(conststr pattern, int placeholder_index);

int width_isolation(conststr placeholder, int length);

#endif //MY_PRINTF_PLACEHOLDERS_ISOLATION_H

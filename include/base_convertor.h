/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_BASE_CONVERTOR_H
    #define MY_PRINTF_BASE_CONVERTOR_H

    #include "my_printf.h"

int str_length(conststr str);

char *reverse_str(char *str);

char *from_decimal(long number, int base);

#endif //MY_PRINTF_BASE_CONVERTOR_H

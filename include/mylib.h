/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_MYLIB_H
    #define MY_PRINTF_MYLIB_H

    #include "base_convertor.h"

char *str_copy(char *dest, char const *src);

void increase_length(char **str, long length);

int is_number(char character);

#endif //MY_PRINTF_MYLIB_H

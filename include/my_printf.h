/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_MY_PRINTF_H
    #define MY_PRINTF_MY_PRINTF_H

typedef char const *conststr;

int my_printf(conststr pattern, ...);

#endif //MY_PRINTF_MY_PRINTF_H

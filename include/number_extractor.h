/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_NUMBER_EXTRACTOR_H
    #define MY_PRINTF_NUMBER_EXTRACTOR_H

int get_number(char const *str);

#endif //MY_PRINTF_NUMBER_EXTRACTOR_H

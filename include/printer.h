/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_PRINTER_H
    #define MY_PRINTF_PRINTER_H
    #define UNSLL_MAX 1000000000000000000

    #include "my_printf.h"
    #include "placeholder.h"

typedef unsigned long long unsll;

void put_char(char character);

void put_unsigned_number(unsll number, params data);

void put_number(long long number, params data);

void put_double(double number, params data);

void put_string(conststr string);

#endif //MY_PRINTF_PRINTER_H

/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_BASE_FORMATTER_H
    #define MY_PRINTF_BASE_FORMATTER_H

    #include <stdarg.h>
    #include "placeholder.h"

int power(int base, int power);

void print_number_octal(va_list args, params data);

void print_number_binary(va_list args, params data);

void print_number_hexadecimal(va_list args, params data);

#endif //MY_PRINTF_BASE_FORMATTER_H

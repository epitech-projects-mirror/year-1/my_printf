/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_PLACEHOLDER_H
    #define MY_PRINTF_PLACEHOLDER_H

    #include <stdarg.h>

typedef struct params_struct {
    char type;
    int width;
    int precision;
    int left_align;
    int explicit_plus;
    int plus_space;
    int fill_zeros;
    int thousands_separator;
    int raw_data;
} params;

typedef struct type_struct {
    char identifier;

    void (*action)(va_list, params);
} type;

type *get_type(char identifier);

#endif //MY_PRINTF_PLACEHOLDER_H

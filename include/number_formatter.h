/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_NUMBER_FORMATTER_H
    #define MY_PRINTF_NUMBER_FORMATTER_H

    #include "placeholder.h"

void print_number(va_list args, params data);

void print_unsigned_number(va_list args, params data);

void print_double_number(va_list args, params data);

#endif //MY_PRINTF_NUMBER_FORMATTER_H

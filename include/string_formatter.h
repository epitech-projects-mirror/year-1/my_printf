/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_STRING_FORMATTER_H
    #define MY_PRINTF_STRING_FORMATTER_H

    #include <stdarg.h>
    #include "placeholder.h"

void print_percentage(va_list args, params data);

void print_string(va_list args, params data);

void print_character(va_list args, params data);

void print_address(va_list args, params data);

void print_printable_string(va_list args, params data);

#endif //MY_PRINTF_STRING_FORMATTER_H

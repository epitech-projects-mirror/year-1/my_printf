/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#ifndef MY_PRINTF_UTILS_H
#define MY_PRINTF_UTILS_H

#include "printer.h"

void put_n_chars(char character, int amount);

int number_length(unsll number);

void print_raw_number(unsll number);

#endif //MY_PRINTF_UTILS_H

/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "mylib.h"
#include <stdlib.h>

char *str_copy(char *dest, char const *src)
{
    int index = 0;

    while (src[index] != '\0') {
        dest[index] = src[index];
        index++;
    }
    dest[index] = '\0';
    return dest;
}

void fill(char **buffer, int length)
{
    if (length > 0) {
        for (int index = 0; index < length; index++)
            (*buffer)[index] = '\0';
    }
}

void increase_length(char **str, long length)
{
    if (length >= 1) {
        int string_length = str_length((*str));
        char *copy = malloc(sizeof(char) * (string_length + 1));

        str_copy(copy, (*str));
        free((*str));
        (*str) = malloc(sizeof(char) * (string_length + length + 1));
        str_copy((*str), copy);
        free(copy);
        for (int index = 0; index < length; index++)
            (*str)[string_length + index] = '\0';
    }
}

int is_number(char character)
{
    return character >= '0' && character <= '9';
}

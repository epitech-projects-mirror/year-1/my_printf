##
## EPITECH PROJECT, 2021
## day10
## File description:
## No file there , just an epitech header example
##

SRC = ./my_printf.c \
	./placeholders_isolation.c \
	./placeholder.c \
	./printer.c \
	./number_formatter.c \
	./string_formatter.c \
	./number_extractor.c \
	./base_formatter.c \
	./base_convertor.c \
	./mylib.c \
	./format_utils.c

TESTS_SRC = ./tests/test_my_printf.c

OBJ = $(SRC:.c=.o)

CRITERION = -g3 --coverage -lcriterion
NAME = printf
CFLAGS += -Werror -Wextra -Iinclude

$(NAME): $(OBJ)
	ar rc libmy.a $(OBJ)

all: $(NAME)

fclean:
	make clean
	rm -f $(NAME) libmy.a unit-tests

clean:
	find . -type f -name '*.o' -delete
	find . -type f -name '*.gcda' -delete
	find . -type f -name '*.gcno' -delete
	find . -type f -name '*.gcov' -delete
	find . -type f -name 'vgcore.*' -delete

re:
	make fclean
	make

tests_run:
	gcc $(SRC) $(TESTS_SRC) $(CRITERION) -o./unit-tests
	./unit-tests

coverage:
	make tests_run
	gcovr -e  tests/

gdb:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g
	gdb --quiet ./$(NAME)

debug:
	gcc $(SRC) main_test.c -o./$(NAME) -g -Iinclude

valgrind:
	gcc $(SRC) -o./$(NAME) -g3 -Iinclude
	valgrind ./$(NAME)

leak:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g3
	valgrind --leak-check=full ./$(NAME)

origins:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g3 -Iinclude
	valgrind --track-origins=yes ./$(NAME)

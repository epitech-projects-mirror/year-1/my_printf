/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include <stdlib.h>
#include "base_convertor.h"
#include "mylib.h"
#include "my_printf.h"

int str_length(conststr str)
{
    int counter = 0;

    if (str == 0 || str[0] == '\0')
        return 0;
    while (str[counter] != '\0')
        counter++;
    return counter;
}

char *reverse_str(char *str)
{
    int length = str_length(str);
    int index = 0;

    if (length <= 1)
        return str;
    for (int to_change = length / 2; to_change > 0; to_change--) {
        char temp = *(str + index);
        *(str + index) = *(str + (length - 1) - index);
        *(str + (length - 1) - index) = temp;
        index++;
    }
    return str;
}

char long_to_char(long number)
{
    return (char) (number <= 9 ? number + 48 : 97 + (number - 10));
}

char *from_decimal(long number, int base)
{
    char *result = malloc(sizeof(char));
    int index = 0;

    result[0] = '\0';
    while (number > 0) {
        increase_length(&result, 1);
        result[index] = long_to_char(number % base);
        number /= base;
        index++;
    }
    result[index] = '\0';
    reverse_str(result);
    return result;
}
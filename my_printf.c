/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my_printf.h"
#include "base_convertor.h"
#include "mylib.h"
#include "placeholder.h"
#include "printer.h"
#include "placeholders_isolation.h"

int contains(conststr str, char character)
{
    for (int index = 0; str[index] != '\0'; index++) {
        if(is_number(str[index]) && str[index] != character)
            return 0;
        if (str[index] == character)
            return 1;
    }
    return 0;
}

int print_from_index(conststr str, int index)
{
    if (index < 0)
        index = 0;
    for (; str[index] != '%' && str[index] != '\0'; index++)
        put_char(str[index]);
    return index;
}

char *extract(conststr pattern, int index, int length)
{
    char *place_holder = malloc(sizeof(char) * (length + 1));

    for (int position = 0; position < length; position++)
        place_holder[position] = pattern[index + position];
    place_holder[length] = '\0';
    return place_holder;
}

params *to_params(conststr placeholder, int length)
{
    params *data = malloc(sizeof(params));

    data->type = placeholder[length - 1];
    data->left_align = (contains(placeholder, '-') ? 1 : 0);
    data->explicit_plus = (contains(placeholder, '+') ? 1 : 0);
    data->plus_space = (contains(placeholder, ' ') ? 1 : 0);
    data->fill_zeros = (contains(placeholder, '0') ? 1 : 0);
    data->thousands_separator = (contains(placeholder, '\'') ? 1 : 0);
    data->raw_data = (contains(placeholder, '#') ? 1 : 0);
    data->width = width_isolation(placeholder, length);
    data->precision = -1;
    return data;
}

int my_printf(conststr pattern, ...)
{
    va_list args;
    type *ph_type;
    int index = 0;

    va_start(args, pattern);
    do {
        index = print_from_index(pattern, index);
        if (index < str_length(pattern) && pattern[index] == '%') {
            int length = placeholder_length(pattern, index);

            if(length > 0) {
                char *place_holder = extract(pattern, index, length);
                params *data = to_params(place_holder, length);
                ph_type = get_type(data->type);

                if (ph_type->action != 0)
                    ph_type->action(args, *data);
                index += length;
                free(place_holder);
                free(data);
                free(ph_type);
            } else {
                put_char(pattern[index]);
                index++;
            }
        }
    } while (pattern[index] != '\0');

    va_end(args);
    return 0;
}

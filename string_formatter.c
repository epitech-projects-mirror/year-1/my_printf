/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "string_formatter.h"
#include "format_utils.h"
#include "printer.h"
#include "base_convertor.h"
#include <unistd.h>

void print_percentage(va_list args, params data)
{
    put_char('%');
}

void print_string(va_list args, params data)
{
    conststr string = va_arg(args, char *);
    if(!data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - str_length(string));
    put_string(string);
    if(data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - str_length(string));
}

void print_character(va_list args, params data)
{
    char character = (char) va_arg(args, int);

    if(!data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - 1);
    write(1, &character, 1);
    if(data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - 1);
}

void print_address(va_list args, params data)
{
    long address = va_arg(args, long);
    conststr converted = from_decimal(address, 16);
    put_string("0x");
    put_string(converted);
}

void print_printable_string(va_list args, params data)
{
    conststr string = va_arg(args, char *);

    for (int index = 0; string[index] != '\0'; index++) {
        char letter = string[index];
        if (letter < 32 || letter >= 127) {
            put_string("\\");
            put_char('0');
            put_string(from_decimal(letter, 8));
        } else {
            write(1, &letter, 1);
        }
    }
}
